#!/usr/local/bin/python
# -*-encoding:UTF-8-*-

# From and input image, try and generate a custom
# kernel-like convolution object that makes the image more like the target

from PIL import Image
import glob

import copy
import math
import random
import os
import signal
import time
import sys

TARGET = False

def print_ker(kernel):
    if not len(kernel):
        raise ValueError("Invalid kernel")
    print "{0:+.4f} {1:+.4f} {2:+.4f}".format(*kernel[0])
    print "{0:+.4f} {1:+.4f} {2:+.4f}".format(*kernel[1])
    print "{0:+.4f} {1:+.4f} {2:+.4f}".format(*kernel[2])

def gpr(s):
    print COLORS['YELLOW'] + '----- %s -----' % s + COLORS['CLEAR']

def pr(s):
    print ' > ' + str(s)

def wr(s):
    sys.stdout.write('\033[0G')
    sys.stdout.flush()
    sys.stdout.write(s)

def progress(count, l, length=40, COL='\033[32m'):
    l = l-1
    p = int((float(count) / l) * length)
    done = '=' * p
    notdone = ' ' * (length - p)
    CLR = '\033[0m'
    s = "[%s%s%s%s] %04d/%04d" % (COL, done, CLR, notdone, count, l)
    wr(s)
    #if count == l:
        #print

def clrscrn():
    print '\033[2J' # blankd screen
    print '\033[f' # move cursor

def destroyscrn():
    print '\033[2J' # blankd screen
    print '\033[f' # move cursor


def normalize(kernel):
    mag = 0.0
    out = kernel
    for x in range(0, len(kernel[0])):
        for y in range(0, len(kernel)):
            mag += abs((kernel[x])[y])
    if mag == 0:
        return None
    for x in range(0, len(kernel[0])):
        for y in range(0, len(kernel)):
             (out[x])[y] /= mag
    return out

A_MAX = 1.0
A_MIN = -1.0
B_MAX = 1.0
B_MIN = -1.0
CE_MAX = 250
CE_MIN = 0.05
DF_MAX = 1.57
DF_MIN = -1.57

MUTATE_FACTOR = 1.0 #0.2

class CosOperator:
    """
    a + b*cos(c*t + d)
    a + b*cos(c*tx + d)*cos(e*ty + f)
    """
    def __init__(self):
        self.a = random.uniform(A_MIN, A_MAX)
        self.b = random.uniform(B_MIN, B_MAX)
        self.c = random.uniform(CE_MIN, CE_MAX)
        self.d = random.uniform(DF_MIN, DF_MAX)
        self.e = random.uniform(CE_MIN, CE_MAX)
        self.f = random.uniform(DF_MIN, DF_MAX)

    def __call__(self, tx, ty):
        return self.a + self.b*math.cos(self.c*tx + self.d)*math.cos(self.e*ty + self.f)

    def validate(self):
        if self.a > A_MAX:
            self.a = A_MAX
        if self.a < A_MIN:
            self.a = A_MIN
        if self.b > B_MAX:
            self.b = B_MAX
        if self.b < B_MIN:
            self.b = B_MIN
        if self.c > CE_MAX:
            self.c = CE_MAX
        if self.c < CE_MIN:
            self.c = CE_MIN
        if self.d > DF_MAX:
            self.d = DF_MAX
        if self.d < DF_MIN:
            self.d = DF_MIN
        if self.e > CE_MAX:
            self.e = CE_MAX
        if self.e < CE_MIN:
            self.e = CE_MIN
        if self.f > DF_MAX:
            self.f = DF_MAX
        if self.f < DF_MIN:
            self.f = DF_MIN


    def mutate(self):
        rand = random.randint(0,len('abcdef')-1)
        if rand == 0:
           self.a += MUTATE_FACTOR * random.uniform(A_MIN, A_MAX)
        elif rand == 1:
           self.b += MUTATE_FACTOR * random.uniform(B_MIN, B_MAX)
        elif rand == 2:
           self.c *= MUTATE_FACTOR * random.random()
        elif rand == 3:
           self.d += MUTATE_FACTOR * random.uniform(DF_MIN, DF_MAX)
        elif rand == 4:
           self.e *= MUTATE_FACTOR * random.random()
        elif rand == 5:
           self.f += MUTATE_FACTOR * random.uniform(DF_MIN, DF_MAX)
        else:
            print "you still fucked up"
        self.validate()

    def pr(self):
        return "<{0:+0.4f} + {1:+0.4f}*cos({2:+0.4f}*t + {3:+0.4f})>".format(self.a, self.b, self.c, self.d)

COLORS = {
    'RED'   : '\033[31m',
    'GREEN' : '\033[32m',
    'YELLOW': '\033[33m',
    'BLUE'  : '\033[34m',
    'CLEAR' : '\033[0m',
}

class RGBGroup:
    def __init__(self):
        self.r = CosOperator()
        self.g = CosOperator()
        self.b = CosOperator()

    def __call__(self, tx, ty):
        return (self.r(tx, ty), self.g(tx, ty), self.b(tx, ty))

    def pr(self):
        return "{RED}r:{0} {GREEN}g:{1} {BLUE}b:{2}{CLEAR}".format(self.r.pr(), self.g.pr(), self.b.pr(), **COLORS)

    def mutate(self):
        rand = random.randint(0,2)
        if rand == 0:
            self.r.mutate()
        elif rand == 1:
            self.g.mutate()
        elif rand == 2:
            self.b.mutate()
        else:
            print "You fucked up"


KER_SIZE = 9
EXTRA_MUTATE_PROB = 0.4

class Kernel_Func:
    def __init__(self):
        self.k = [
                [RGBGroup(), RGBGroup(), RGBGroup()],
                [RGBGroup(), RGBGroup(), RGBGroup()],
                [RGBGroup(), RGBGroup(), RGBGroup()],
                ]

    def __call__(self, tx, ty):
        outk = []
        for row in self.k:
            thisrow = []
            for col in row:
                thisrow.append(col(tx,ty))
            outk.append(thisrow)
        return outk

    def s(self):
        s = ''
        for ky in self.k:
            for kx in ky:
                s +=  kx.pr() + '\n'
            s += '\n'
        return s

    def pr(self):
        for ky in self.k:
            for kx in ky:
                print kx.pr()
            print

    def mutate(self, i):
        y = i / 3
        x = i % 3
        self.k[y][x].mutate()

    def reproduce(self, num=6):
        children = []
        for i in range(num):
            children.append(copy.deepcopy(self))
        for child in children:
            r = random.randint(0,KER_SIZE-1)
            child.mutate(r)
            while (random.random() > EXTRA_MUTATE_PROB):
                child.mutate(r)
        return children

    def convolve_with_func(self, img):
        rgb_im = img.convert('RGB')
        width, height = img.size
        kw = 3
        kh = 3
        output = Image.new('RGB', (width, height), "black")
        pix = img.load()
        out = output.load()
        offs = kw / 2 - 1
        for y in range(0, height):
            ty = float(y) / height
            progress(y, height)
            for x in range(0, width):
                tx = float(x) / width
                k = self(tx,ty)
                r1, g1, b1 = 0, 0, 0
                for kx in range(0, kw):
                    for ky in range(0, kh):

                        xi = x + kx - offs
                        yi = y + ky - offs
                        if xi < 0:
                            xi = 0
                        if xi >= width-1:
                            xi = width-1
                        if yi < 0:
                            yi = 0
                        if yi >= height-1:
                            yi = height-1
                        r,g,b = pix[xi, yi]
                        ki = (k[kx])[ky]
                        r1 += r * ki[0]
                        g1 += g * ki[1]
                        b1 += b * ki[2]
                out[x, y] = (int(abs(r1)), int(abs(g1)), int(abs(b1)))
        print
        return output

class Target:
    def __init__(self, img):
        with open('target.bmp') as tf:
            pr('Generating target fitness')
            timg = Image.open(tf)
            xd = []
            pix = timg.load()
            w, h = timg.size
            self.xdr = []
            self.xdg = []
            self.xdb = []
            self.ydr = []
            self.ydg = []
            self.ydb = []
            for x in range(0, w):
                self.xdr.append(0)
                self.xdg.append(0)
                self.xdb.append(0)
                progress(x, w, COL=COLORS['BLUE'])
                for y in range(0, h):
                    r,g,b = pix[x, y]
                    self.xdr[x] += r
                    self.xdg[x] += g
                    self.xdb[x] += b
            for y in range(0, h):
                self.ydr.append(0)
                self.ydg.append(0)
                self.ydb.append(0)
                progress(y, h, COL=COLORS['BLUE'])
                for x in range(0, w):
                    r,g,b= pix[x, y]
                    self.ydr[y] += r
                    self.ydg[y] += g
                    self.ydb[y] += b


def fitness(img):
    global TARGET
    if not TARGET:
        TARGET = Target(img)

    pix = img.load()
    w, h = img.size
    xdr = []
    xdg = []
    xdb = []
    ydr = []
    ydg = []
    ydb = []
    for x in range(0, w):
        xdr.append(0)
        xdg.append(0)
        xdb.append(0)
        progress(x, w+h, COL=COLORS['BLUE'])
        for y in range(0, h):
            r,g,b= pix[x, y]
            xdr[x] += r
            xdg[x] += g
            xdb[x] += b
    for y in range(0, h):
        ydr.append(0)
        ydg.append(0)
        ydb.append(0)
        progress(y+w, w+h, COL=COLORS['BLUE'])
        for x in range(0, w):
            r,g,b= pix[x, y]
            ydr[y] += r
            ydg[y] += g
            ydb[y] += b

    fitness = 0
    for x in range(w):
        fitness += abs(TARGET.xdr[x] - xdr[x]) / 3
        fitness += abs(TARGET.xdg[x] - xdg[x]) / 3
        fitness += abs(TARGET.xdb[x] - xdb[x]) / 3
    for y in range(h):
        fitness += abs(TARGET.ydr[y] - ydr[y]) / 3
        fitness += abs(TARGET.ydg[y] - ydg[y]) / 3
        fitness += abs(TARGET.ydb[y] - ydb[y]) / 3
    print '     %s' % fitness
    return fitness



import datetime
import time

best_kernel = None

def run():
    gpr('Running')
    with open('cat.bmp') as f:
        with open ('log.txt', 'a') as lf:
            def log(s):
                st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                lf.write('%s%s%s -- %s\n' % (COLORS['YELLOW'], st, COLORS['CLEAR'], s))
            def saveimg(img, extra=""):
                if extra:
                    extra = '_' + extra
                #num = random.randint(0,10000)
                num = len(os.listdir('./done'))
                log('saving %d' % num)
                img.save('done/%05d%s.png' % (num, extra))
            log('starting new session')
            img = Image.open(f)
            base_fit = fitness(img)
            k = Kernel_Func()
            pr('base fitness: %s' % base_fit)
            log('base kernel: \n%s = %d' % (str(k.s()), base_fit))

            k_c_fitnesses = []
            normal = k.convolve_with_func(img)
            best_fitness = fitness(normal)
            saveimg(normal)

            global best_kernel
            #best_kernel = None

            destroyscrn()
            #best_fitness = base_fit
            generation = 0
            while (True):
                gpr('Generation %05d' % generation)
                pr('best fitness so far: %d' % best_fitness)
                generation += 1
                if generation % 20 == 0:
                    final = best_kernel.convolve_with_func(img)
                    saveimg(final, extra='g%d'%(generation))
                k_children = k.reproduce()
                best_index = 0
                i = 0 # this means base is best
                for child in k_children:
                    i += 1
                    child_conv = child.convolve_with_func(img)
                    f = fitness(child_conv)
                    log('gen %d child %d: \n%s\n = %d' % (generation, i, str(child.s()), f))
                    if f <= best_fitness: # lower fitness is better
                        best_index = i
                        best_fitness = f
                        best_kernel = child
                    #saveimg(child_conv, extra='g%dc%d'%(generation,i))
                clrscrn()
                if best_index > 0:
                    pr('child %d was best with fitness %d, mutating from it' % (best_index, best_fitness))
                    k = k_children[best_index-1]
                else:
                    pr('all children were worse, remutating')


def exit_gracefully(signum, frame):
    signal.signal(signal.SIGINT, original_sigint)
    try:
        if raw_input("\nReally quit? (y/n)> ").lower().startswith('y'):
            print 'saving best...'
            with open('cat.bmp') as f:
                img = Image.open(f)
                final = best_kernel.convolve_with_func(img)
                final.save('bestest.png')
            sys.exit(1)
    except KeyboardInterrupt:
        with open('cat.bmp') as f:
            img = Image.open(f)
            final = best_kernel.convolve_with_func(img)
            final.save('bestest.png')
        sys.exit(1)
    signal.signal(signal.SIGINT, exit_gracefully)

if __name__ == '__main__':
    original_sigint = signal.getsignal(signal.SIGINT)
    signal.signal(signal.SIGINT, exit_gracefully)
    run()
